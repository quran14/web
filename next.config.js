const million = require("million/compiler");

const { withAxiom } = require("next-axiom");
/** @type {import('next').NextConfig} */
const nextConfig = withAxiom({
  experimental: {
    serverActions: true,
  },
  async redirects() {
    return [
      {
        source: "/maintenance",
        destination: "/",
        permanent: false,
      },
    ];
  },
  reactStrictMode: true,
});

const millionConfig = {
  auto: true,
  // if you're using RSC:
  auto: { rsc: true },
};

module.exports = million.next(nextConfig, millionConfig);
