export type JuzWithLatestQuran =
  | {
      id: string;
      number: number;
      name: string;
      bookedBy: string | null;
      quranId: number;
    }[]
  | undefined;