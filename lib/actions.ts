"use server";

import { CreateResponse } from "@/app/api/create/route";
import { revalidatePath } from "next/cache";
import {
  sendOnError,
  sendQuranCompletionMail,
  sendWholeQuranTakenMail,
} from "./email";
import prisma from "./prisma";

export async function linkUserToFullQuran(username: string) {
  try {
    const newQuran = await prisma.quran.create({
      data: { singlePerson: true },
    });

    // Perfomant Query
    await prisma.juzz.create({
      data: {
        bookedBy: username,
        number: 1,
        name: "All Juzz",
        quranId: newQuran.id,
      },
    });
    sendWholeQuranTakenMail(newQuran, username);
    return { status: 200, message: null, quranNo: newQuran.id };
  } catch (error) {
    sendOnError(error);
    console.error("Err: Something went wrong", error);
    return { status: 404, message: "INTERNAL_ERROR" };
  }
}

export async function linkUserToJuzz(
  quranId: number,
  username: string,
  juzzNumber: number,
  juzzId: string
) {
  try {
    const thatJuzz = await prisma.juzz.findUnique({
      where: { quranId: quranId, number: juzzNumber, id: juzzId },
    });
    if (!thatJuzz?.bookedBy) {
      const lastJuzz = await prisma.juzz.update({
        where: { quranId: quranId, number: juzzNumber, id: juzzId },
        data: { bookedBy: username },
        select: {
          quran: true,
          bookedBy: true,
          id: true,
          name: true,
          number: true,
          quranId: true,
        },
      });
      const allJuzz = await prisma.juzz.findMany({
        where: { quranId: quranId },
      });
      // Check if allJuzz has any empty slots (bookedBy === null)
      const hasEmptySlots = allJuzz.some((juzz) => juzz.bookedBy === null);
      if (!hasEmptySlots) {
        const res = await startNewQuran();
        if (quranId % 10 === 0) {
          sendQuranCompletionMail(res, lastJuzz);
        }
        // Marks page for revalidating
        if (res.success) {
          revalidatePath("/");
          return { status: 200, message: null };
        }
        return { status: 404, message: "INTERNAL_ERROR" };
      }
      // Marks page for revalidating
      revalidatePath("/");
      return { status: 200, message: null };
    } else {
      // Marks page for revalidating
      revalidatePath("/");
      console.error("Err: Slot has been filled!");
      return { status: 403, message: "SLOT_FILLED" };
    }
  } catch (error) {
    sendOnError(error);
    console.error("Err: Something went wrong", error);
    return { status: 404, message: "INTERNAL_ERROR" };
  }
}

async function startNewQuran() {
  const url = process.env.NEXT_PUBLIC_URL ?? "http://localhost:3000/";
  const res = await fetch(`${url}/api/create`, {
    method: "POST",
  });
  const data: CreateResponse = await res.json();
  return data;
}
