import { CreateResponse } from "@/app/api/create/route";
import { formatDate } from "@/app/page";
import { quran } from "@prisma/client";
import { Resend } from "resend";
const resend = new Resend(process.env.RESEND_KEY);

export async function sendQuranCompletionMail(
  data: CreateResponse,
  juzz: {
    number: number;
    quran: {
      id: number;
      createdAt: Date;
      updatedAt: Date;
      singlePerson: boolean;
    };
    id: string;
    name: string;
    bookedBy: string | null;
    quranId: number;
  }
) {
  if (!process.env.RESEND_FROM || !process.env.RESEND_TO) {
    console.log("ENV missing [RESEND_FROM] | [RESEND_TO]");
    return;
  }
  const completedQuranNo = juzz.quranId;

  const startedTime = () => {
    if (!juzz.quran.createdAt) {
      return "N/A";
    }
    return formatDate(new Date(juzz.quran.createdAt));
  };

  const endedTime = () => {
    if (!data.createdAt) {
      return "N/A";
    }
    return formatDate(new Date(data.createdAt));
  };

  const template = `<p><a target="_blank" rel="noopener noreferrer nofollow" href="https://quran14k.com/${completedQuranNo}">Quran No. ${completedQuranNo}</a> is completed and <a target="_blank" rel="noopener noreferrer nofollow" href="https://quran14k.com">New Quran No. ${
    data.quranNo
  }</a>. has been started. </p><p>Last Juzz was ${
    juzz.number
  } and was booked by: <strong>${
    juzz.bookedBy
  }</strong>.</p> <hr><ul><li><p>Details</p></li></ul><p>Started on - <strong>${startedTime()}</strong></p><p>and ended on - <strong>${endedTime()}</strong></p>`;

  const result = await resend.emails.send({
    from: process.env.RESEND_FROM,
    to: process.env.RESEND_TO,
    subject: `Quran ${completedQuranNo} completed!`,
    html: template,
  });
  console.log(result);
}

export async function sendWholeQuranTakenMail(quran: quran, takenBy: string) {
  if (!process.env.RESEND_FROM || !process.env.RESEND_TO) {
    console.log("ENV missing [RESEND_FROM] | [RESEND_TO]");
    return;
  }

  const result = await resend.emails.send({
    from: process.env.RESEND_FROM,
    to: process.env.RESEND_TO,
    subject: `Quran ${quran.id} taken!`,
    html: `<p><a target="_blank" rel="noopener noreferrer nofollow" href="https://quran14k.com/${quran.id}">Quran No. ${quran.id}</a> was taken by a single person - <strong>${takenBy}</strong></p>`,
  });
  console.log(result);
}

export async function sendOnError(error: any) {
  if (!process.env.RESEND_FROM || !process.env.RESEND_TO) {
    console.log("ENV missing [RESEND_FROM] | [RESEND_TO]");
    return;
  }

  const result = await resend.emails.send({
    from: process.env.RESEND_FROM,
    to: process.env.RESEND_TO,
    subject: `Error occurred`,
    html: `<p>An error occurred</p><hr><p>Error: ${error}</p>`,
  });
  console.log(result);
}
