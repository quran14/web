export function generateJuzz(quranId: number) {
  return [
    {
      number: 1,
      quranId: quranId,
      name: "Juzz 1",
    },
    {
      number: 2,
      quranId: quranId,
      name: "Juzz 2",
    },
    {
      number: 3,
      quranId: quranId,
      name: "Juzz 3",
    },
    {
      number: 4,
      quranId: quranId,
      name: "Juzz 4",
    },
    {
      number: 5,
      quranId: quranId,
      name: "Juzz 5",
    },
    {
      number: 6,
      quranId: quranId,
      name: "Juzz 6",
    },
    {
      number: 7,
      quranId: quranId,
      name: "Juzz 7",
    },
    {
      number: 8,
      quranId: quranId,
      name: "Juzz 8",
    },
    {
      number: 9,
      quranId: quranId,
      name: "Juzz 9",
    },
    {
      number: 10,
      quranId: quranId,
      name: "Juzz 10",
    },
    {
      number: 11,
      quranId: quranId,
      name: "Juzz 11",
    },
    {
      number: 12,
      quranId: quranId,
      name: "Juzz 12",
    },
    {
      number: 13,
      quranId: quranId,
      name: "Juzz 13",
    },
    {
      number: 14,
      quranId: quranId,
      name: "Juzz 14",
    },
    {
      number: 15,
      quranId: quranId,
      name: "Juzz 15",
    },
    {
      number: 16,
      quranId: quranId,
      name: "Juzz 16",
    },
    {
      number: 17,
      quranId: quranId,
      name: "Juzz 17",
    },
    {
      number: 18,
      quranId: quranId,
      name: "Juzz 18",
    },
    {
      number: 19,
      quranId: quranId,
      name: "Juzz 19",
    },
    {
      number: 20,
      quranId: quranId,
      name: "Juzz 20",
    },
    {
      number: 21,
      quranId: quranId,
      name: "Juzz 21",
    },
    {
      number: 22,
      quranId: quranId,
      name: "Juzz 22",
    },
    {
      number: 23,
      quranId: quranId,
      name: "Juzz 23",
    },
    {
      number: 24,
      quranId: quranId,
      name: "Juzz 24",
    },
    {
      number: 25,
      quranId: quranId,
      name: "Juzz 25",
    },
    {
      number: 26,
      quranId: quranId,
      name: "Juzz 26",
    },
    {
      number: 27,
      quranId: quranId,
      name: "Juzz 27",
    },
    {
      number: 28,
      quranId: quranId,
      name: "Juzz 28",
    },
    {
      number: 29,
      quranId: quranId,
      name: "Juzz 29",
    },
    {
      number: 30,
      quranId: quranId,
      name: "Juzz 30",
    },
  ];
}

export function generateExceptionalSingleJuzz(
  quranId: number,
  username: string
) {
  return {
    number: 1,
    quranId: quranId,
    name: "All Juzz",
    bookedBy: username,
  };
}
