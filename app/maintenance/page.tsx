import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";

export default function Maintenance() {
  return (
    <main className="flex min-h-screen flex-col md:items-center p-4">
      <Card>
        <CardHeader>
          <CardTitle>Maintenance Mode</CardTitle>
          <CardContent className="text-md">
            We are currently addressing some issues with the website. Please
            wait until they are resolved.
          </CardContent>
        </CardHeader>
      </Card>
    </main>
  );
}
