import prisma from "@/lib/prisma";
import { JuzWithLatestQuran } from "@/types/JuzzWithLatestQuran";
import { NextResponse } from "next/server";

export type GetAllJuzzResponse = {
  success: boolean;
  juzz?: JuzWithLatestQuran;
  quranNo?: number;
  singlePerson: boolean;
  createdAt?: Date;
  endedAt?: Date;
};

export async function GET() {
  const latestQuran = await prisma.quran.findFirst({
    where: { singlePerson: false },
    orderBy: { createdAt: "desc" },
    include: {
      Juzz: {
        orderBy: [
          { bookedBy: { sort: "asc", nulls: "first" } },
          { number: "asc" },
        ],
      },
    },
  });

  console.log("Latest Quran: ", latestQuran);

  if (!latestQuran) {
    console.error("No Quran found");
    return NextResponse.json({
      success: false,
      juzz: null,
      quranNo: null,
      singlePerson: false,
      createdAt: null,
    });
  }

  const juzzList = latestQuran?.Juzz ?? [];

  const juzz: JuzWithLatestQuran = juzzList.sort((a, b) => {
    if (a.bookedBy === null && b.bookedBy !== null) {
      return -1; // null bookedBy values at start
    }
    if (a.bookedBy !== null && b.bookedBy === null) {
      return 1; // non-null bookedBy values after null
    }
    return a.number - b.number;
  });

  return NextResponse.json({
    success: true,
    juzz: juzz,
    quranNo: latestQuran.id,
    singlePerson: latestQuran.singlePerson,
    createdAt: latestQuran.createdAt,
  });
}
