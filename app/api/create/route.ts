import { generateJuzz } from "@/lib/generateJuzzs";
import prisma from "@/lib/prisma";
import { log } from "next-axiom";
import { NextResponse } from "next/server";

export type CreateResponse = {
  success: boolean;
  quranNo?: number;
  message?: string;
  createdAt?: string;
  isSinglePerson?: boolean;
};

export async function POST() {
  console.log("Creating new Quran and Juzz...");
  try {
    const newQuran = await prisma.quran.create({
      data: {},
    });
    console.log("New Quran created successfully!");
    const juzz30 = generateJuzz(newQuran.id);
    await prisma.juzz.createMany({
      data: juzz30,
    });

    console.log("New Quran and Juzz created successfully!");

    return NextResponse.json({
      success: true,
      quranNo: newQuran.id,
      message: null,
      createdAt: newQuran.createdAt,
      isSinglePerson: newQuran.singlePerson,
    });
  } catch (error) {
    console.log("Error: Something went wrong", error);
    log.error(`Quran and Juzz creation went wrong, ${error}`);
    return NextResponse.json({
      success: false,
      quranNo: null,
      message: `${error}`,
      createdAt: null,
      isSinglePerson: null,
    });
  }
}
