import prisma from "@/lib/prisma";
import { JuzWithLatestQuran } from "@/types/JuzzWithLatestQuran";
import { NextResponse } from "next/server";

export async function GET(request: Request) {
  const { searchParams } = new URL(request.url);

  const quranId = searchParams.get("id") as string;

  const quran = await prisma.quran.findFirst({
    where: { id: Number(quranId) },
    include: {
      Juzz: {
        orderBy: [{ number: "asc" }],
      },
    },
  });

  const nextQuran = await prisma.quran.findFirst({
    where: {
      id: { gt: Number(quranId) },
      singlePerson: false,
    },
    orderBy: { id: "asc" },
  });

  if (!quran) {
    return NextResponse.json({
      success: false,
      juzz: [],
      quranNo: quranId,
      singlePerson: false,
      createdAt: null,
    });
  }

  const juzz: JuzWithLatestQuran = quran?.Juzz;

  return NextResponse.json({
    success: true,
    juzz: juzz,
    quranNo: quranId,
    singlePerson: quran?.singlePerson,
    createdAt: quran.createdAt,
    endedAt: nextQuran?.createdAt,
  });
}
