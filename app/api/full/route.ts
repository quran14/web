import prisma from "@/lib/prisma";
import { log } from "next-axiom";
import { NextResponse } from "next/server";

export async function GET(request: Request) {
  try {
    const { searchParams } = new URL(request.url);

    const username = searchParams.get("username") as string;

    const newQuran = await prisma.quran.create({
      data: { singlePerson: true },
    });

    // Perfomant Query
    await prisma.juzz.create({
      data: {
        bookedBy: username,
        number: 1,
        name: "All Juzz",
        quranId: newQuran.id,
      },
    });

    console.log("New Quran and Juzz created successfully!");

    return NextResponse.json({
      success: true,
      quranNo: newQuran.id,
      message: null,
    });
  } catch (error) {
    log.error(`Quran and Juzz creation went wrong, ${error}`);
    return NextResponse.json({
      success: false,
      quranNo: null,
      message: `${error}`,
    });
  }
}
