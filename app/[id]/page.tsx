import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import prisma from "@/lib/prisma";
import { ArrowLeftCircle, ArrowRightCircle, HomeIcon } from "lucide-react";
import Link from "next/link";
import { notFound } from "next/navigation";
import { GetAllJuzzResponse } from "../api/all/route";
import QuranTable from "../component/QuranTable";
import { ModeToggle } from "../component/ThemeToggle";
import { formatDate } from "../page";

export const dynamic = "force-static",
  dynamicParams = true;

async function getData(id: number) {
  const url = process.env.NEXT_PUBLIC_URL ?? "http://localhost:3000";
  const res = await fetch(`${url}/api/quran?id=${id}`, {
    cache: "no-cache",
  });
  const data: GetAllJuzzResponse = await res.json();
  return data;
}

export default async function List({
  params,
  searchParams,
}: {
  params: { id: string };
  searchParams?: { [latest: string]: string };
}) {
  const response = await getData(Number(params.id));
  if (!response.success) {
    return notFound();
  }

  return (
    <main className="flex min-h-screen flex-col md:items-center p-4">
      {/* {params?.id === "63" && (
        <p className="tracking-wide font-semibold text-red-400">
          Due to a technical error, some Juzz of Quran No.{params.id} were not
          filled, and as a result, they are empty.
        </p>
      )} */}
      <p className="text-2xl text-center">Quran {params?.id}</p>
      {response.singlePerson ? (
        <>
          {response?.createdAt && (
            <time dateTime={new Date(response.createdAt).toDateString()}>
              <p className="text-sm text-center font-mono">
                Confirmed {formatDate(new Date(response.createdAt))}
              </p>
            </time>
          )}
        </>
      ) : (
        <>
          {response?.createdAt && (
            <time dateTime={new Date(response.createdAt).toDateString()}>
              <p className="text-sm text-center font-mono">
                Started {formatDate(new Date(response.createdAt))}
              </p>
            </time>
          )}
          {response?.endedAt && (
            <time dateTime={new Date(response.endedAt).toDateString()}>
              <p className="text-sm text-center font-mono">
                All Juzz Confirmed {formatDate(new Date(response.endedAt))}
              </p>
            </time>
          )}
        </>
      )}

      <div className="flex flex-row justify-between">
        {Number(params.id) > 1 && (
          <Link
            href={`/${(Number(params.id) - 1).toString()}?latest=${
              searchParams?.latest
            }`}
          >
            <Button className="m-2" variant={"outline"}>
              <div className="flex flex-row space-x-2 items-center">
                <ArrowLeftCircle className="w-4 h-4" />
                <p>Previous</p>
              </div>
            </Button>
          </Link>
        )}
        {Number(searchParams?.latest) === Number(params.id) + 1 ? (
          <Link href={`/`}>
            <Button className="m-2" variant={"outline"}>
              <div className="flex flex-row space-x-2 items-center">
                <HomeIcon className="w-4 h-4" />
                <p>Next</p>
              </div>
            </Button>
          </Link>
        ) : (
          <Link
            href={`/${(Number(params.id) + 1).toString()}?latest=${
              searchParams?.latest
            }`}
          >
            {searchParams?.latest &&
              searchParams.latest !== undefined &&
              isNumeric(searchParams.latest) && (
                <Button
                  className="m-2"
                  disabled={response.success && response.juzz?.length === 0}
                  variant={"outline"}
                >
                  <div className="flex flex-row space-x-2 items-center">
                    <ArrowRightCircle className="w-4 h-4" />
                    <p>Next</p>
                  </div>
                </Button>
              )}
          </Link>
        )}
      </div>

      {response.juzz?.length === 0 && <p>Not found</p>}

      {response.juzz && response.singlePerson ? (
        <Card>
          <CardHeader>
            <CardTitle>Confirmed</CardTitle>
            <CardDescription>
              All 30 juzz of Quran No.{params.id} is taken by
            </CardDescription>
            <CardContent className="text-xl font-semibold tracking-wide">
              {response.juzz.at(0)?.bookedBy}
            </CardContent>
          </CardHeader>
        </Card>
      ) : (
        <QuranTable juzz={response.juzz} />
      )}

      <div className="py-2 justify-center self-center items-center flex">
        <ModeToggle />
      </div>
    </main>
  );
}

function isNumeric(input: string): boolean {
  return !isNaN(Number(input));
}

export async function generateStaticParams() {
  const quranIds = await prisma.quran.findMany({ select: { id: true } });
  return quranIds.map((q) => ({
    id: q.id.toString(),
  }));
}
