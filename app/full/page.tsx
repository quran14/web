import { Card, CardFooter, CardHeader, CardTitle } from "@/components/ui/card";
import FullQuranNameEditor from "../component/FullQuranNameEditor";

export default function Page() {
  return (
    <main className="flex min-h-screen flex-col md:items-center p-4">
      <Card>
        <CardHeader>
          <CardTitle className="text-xl tracking-wide underline">
            Add your name if you wish to read a full Quran (30 Juzz)
          </CardTitle>
        </CardHeader>
        <CardFooter>
          <FullQuranNameEditor />
        </CardFooter>
      </Card>
    </main>
  );
}
