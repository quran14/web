import Link from "next/link";

export default function NotFound() {
  return (
    <main className="flex min-h-screen flex-col md:items-center p-4">
      <p className="text-sm">
        Oops! Not found. The Quran number you mentioned may have not been
        started yet!
      </p>

      <Link href={"/"} className="underline">
        Go to home page
      </Link>
    </main>
  );
}
