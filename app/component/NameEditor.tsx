"use client";
import { Input } from "@/components/ui/input";
import { linkUserToJuzz } from "@/lib/actions";
import { juzz } from "@prisma/client";
import { CheckCircledIcon } from "@radix-ui/react-icons";
import { Loader2 } from "lucide-react";
import { useEffect, useState, useTransition } from "react";
import { toast } from "react-hot-toast";

export default function NameEditor({ juzz }: { juzz: juzz }) {
  const [name, setName] = useState("");
  const [iconClicked, setIconClicked] = useState(false);
  const [error, setError] = useState<string | null>("");
  let [isPending, startTransition] = useTransition();

  useEffect(() => {
    if (error) {
      toast.error(error);
      setIconClicked(true);
      setError(null);
    }
  }, [error]);

  return (
    <div className="flex flex-row space-x-4 items-center">
      <Input
        value={juzz?.bookedBy ?? name}
        placeholder="Your Name"
        onChange={(e) => {
          setName(e.target.value);
        }}
        disabled={juzz?.bookedBy !== null}
        className={`${juzz?.bookedBy === null && "border border-foreground"}`}
      />
      {isPending && <Loader2 className="w-6 h-6 animate-spin" />}
      {name.length !== 0 && !iconClicked && (
        <CheckCircledIcon
          onClick={() => {
            startTransition(async () => {
              if (!iconClicked) {
                const response = await linkUserToJuzz(
                  juzz.quranId,
                  name,
                  juzz.number,
                  juzz.id
                );
                if (response.status === 200) {
                  toast.success(
                    `JazakAllah! Your name has been added to Juzz ${juzz.number}.`,
                    {
                      duration: 6000,
                      position: "bottom-center",
                      icon: "📌",
                    }
                  );
                  setIconClicked(true);
                } else {
                  if (response?.message === "SLOT_FILLED") {
                    setError(
                      "Someone has already taken the Juzz. Please select another!"
                    );
                  } else {
                    setError("Something went wrong, Please try again later!");
                  }
                }
              }
            });
          }}
          className="w-8 h-8 hover:cursor-pointer hover:text-blue-600 animate-pulse"
        />
      )}
    </div>
  );
}
