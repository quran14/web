"use client";

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import NameEditor from "./NameEditor";

import { JuzWithLatestQuran } from "@/types/JuzzWithLatestQuran";
import Image from "next/image";

export default function QuranTable({ juzz }: { juzz: JuzWithLatestQuran }) {
  return (
    <div className="px-6 sm:px-24 md:w-3/6">
      <Table>
        <TableHeader>
          <TableRow>
            <TableHead className="font-bold">Juzz</TableHead>
            <TableHead className="font-bold">Confirmed</TableHead>
            <TableHead className="font-bold">Reader</TableHead>
          </TableRow>
        </TableHeader>
        <TableBody>
          {juzz?.map((e) => {
            return (
              <TableRow key={e.number}>
                <TableCell className="font-bold">{e.number}</TableCell>
                <TableCell className="flex justify-center">
                  {e.bookedBy !== null ? (
                    <Image
                      src={"/check.png"}
                      width={20}
                      height={20}
                      alt="booked"
                    />
                  ) : (
                    <Image
                      src={"/wrong.png"}
                      width={20}
                      height={20}
                      alt="booked"
                    />
                  )}
                </TableCell>
                <TableCell className="font-medium w-2/3 text-lg">
                  <NameEditor juzz={e} />
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </div>
  );
}
