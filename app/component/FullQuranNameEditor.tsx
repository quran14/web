"use client";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { linkUserToFullQuran } from "@/lib/actions";
import { CheckCircledIcon } from "@radix-ui/react-icons";
import { Loader2 } from "lucide-react";
import Link from "next/link";
import { useEffect, useState, useTransition } from "react";
import { toast } from "react-hot-toast";

export default function FullQuranNameEditor() {
  const [name, setName] = useState("");
  const [iconClicked, setIconClicked] = useState(false);
  const [error, setError] = useState<string | null>("");
  let [isPending, startTransition] = useTransition();
  const [data, setData] = useState<
    | {
        status: number;
        message: null;
        quranNo: number;
      }
    | {
        status: number;
        message: string;
        quranNo?: undefined;
      }
  >();

  useEffect(() => {
    if (error) {
      toast.error(error);
      setIconClicked(true);
      setError(null);
    }
  }, [error]);

  return (
    <div className="flex flex-col gap-2">
      {data?.status === 200 && data.quranNo ? (
        <p className="tracking-wide font-semibold text-sm">
          Your name has been added to Quran No.{data?.quranNo}. JazakAllah!
        </p>
      ) : (
        <>
          <div className="flex flex-row space-x-4 items-center">
            <Input
              value={name}
              placeholder="Your Name"
              onChange={(e) => {
                setName(e.target.value);
              }}
              className={"border border-foreground"}
              disabled={iconClicked}
            />
            {isPending && <Loader2 className="w-6 h-6 animate-spin" />}
            {name.length !== 0 && !iconClicked && (
              <CheckCircledIcon
                onClick={() => {
                  startTransition(async () => {
                    if (!iconClicked) {
                      const response = await linkUserToFullQuran(name);
                      setData(response);
                      if (response.status === 200) {
                        toast.success("Success", {
                          duration: 3000,
                          position: "bottom-center",
                        });
                        setIconClicked(true);
                      } else {
                        setError(
                          "Something went wrong, Please try again later!"
                        );
                      }
                    }
                  });
                }}
                className="w-8 h-8 hover:cursor-pointer hover:text-blue-600 animate-pulse"
              />
            )}
          </div>
        </>
      )}
      {data?.status === 200 && data.quranNo && (
        <Link href={"/"}>
          <Button variant={"outline"}>Go to Home Page</Button>
        </Link>
      )}
    </div>
  );
}
