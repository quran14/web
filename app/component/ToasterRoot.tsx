"use client";

import { Toaster } from "react-hot-toast";

export default function ToasterRoot() {
  return <Toaster />;
}
