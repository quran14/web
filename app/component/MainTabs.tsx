"use client";

import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { GithubIcon } from "lucide-react";
import Link from "next/link";
import { AboutDialog } from "./AboutDialog";
import QuranNavigationEditor from "./QuranNavigationEditor";
import { ModeToggle } from "./ThemeToggle";

export default function MainTabs({ latestQuranNo }: { latestQuranNo: number }) {
  return (
    <Tabs defaultValue="aim" className="max-w-lg max-h-min">
      <TabsList>
        <TabsTrigger value="aim">Our Aim</TabsTrigger>
        <TabsTrigger value="merits">Merits</TabsTrigger>
        <TabsTrigger value="settings">Settings</TabsTrigger>
      </TabsList>
      <div className="h-min max-w-sm">
        <TabsContent value="aim">
          <div className="flex flex-col gap-2 justify-center items-center">
            <Card>
              <CardHeader>
                <CardTitle>Quran No {latestQuranNo}</CardTitle>
                <CardContent className="text-md">
                  We aim to complete 14,000 Qurans before Arbaeen and gift it to
                  Imam Hussein (a.s.). You are invited to consider yourself
                  fortunate as you select a Juzz(s) and add your name.
                </CardContent>
              </CardHeader>
              <CardFooter>
                ہمارا مقصد ہے کہ چودہ ہزار قرآن مکمل کرنے کی کوشش کریں اربعین سے
                پہلے۔ آپ کو مدعو کیا جاتا ہے کہ اپنے آپ کو خوش قسمت سمجھیں جب آپ
                ایک جز انتخاب کریں اور اپنا نام شامل کریں۔
              </CardFooter>
            </Card>
            <Link href={"/full"}>
              <Button>Click here if you wish to read a full Quran</Button>
            </Link>
          </div>
        </TabsContent>
        <TabsContent value="merits">
          <Card>
            <CardHeader>
              <CardTitle className="leading-6 font-medium">
                Imam Ja&apos;far ibne Muhammad as-Sadiq (pbuh) said
              </CardTitle>
              <CardDescription>
                قَراءَةُ الْقُرآنِ فِي الْمُصْحَفِ تُخَفِّفُ الْعَذابَ عَنِ
                الْوالِدَينِ وَ لَوْ كانا كافِرَينِ
              </CardDescription>
              <CardContent className="text-md font-semibold tracking-wide">
                Reciting the Qur&apos;an from the pages of the Qur&apos;an
                (meaning to look at it and recite it - not from memory) lightens
                the punishment of one&apos;s mother and father, even if they are
                both disbelievers.
              </CardContent>
            </CardHeader>
            <CardFooter>
              <p>Usulul Kafi, Volume 2, Page 613</p>
            </CardFooter>
          </Card>
        </TabsContent>
        <TabsContent value="settings">
          <Card>
            <CardHeader>
              <CardTitle>Settings</CardTitle>
              <CardContent>
                <div className="w-full py-2 flex justify-between items-center">
                  <ModeToggle />
                  <Link href={"https://gitlab.com/quran14/web"} target="_blank">
                    <Button variant="outline" size="icon">
                      <GithubIcon className="h-[1.2rem] w-[1.2rem]" />
                    </Button>
                  </Link>
                  <AboutDialog />
                </div>
              </CardContent>
            </CardHeader>
            <CardFooter>
              <div className="flex flex-col gap-2">
                <p className="text-primary text-base font-semibold">
                  Directly go to Quran No
                </p>
                <p className="text-primary text-sm">
                  Enter number from 1 to {latestQuranNo}
                </p>
                <QuranNavigationEditor latestQuranNo={latestQuranNo} />
              </div>
            </CardFooter>
          </Card>
        </TabsContent>
      </div>
    </Tabs>
  );
}
