"use client";

import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import { InfoCircledIcon } from "@radix-ui/react-icons";
import { useState } from "react";

export function AboutDialog() {
  const [open, setOpen] = useState(false);
  return (
    <>
      <Button
        variant="outline"
        size="icon"
        onClick={() => {
          setOpen(!open);
        }}
      >
        <InfoCircledIcon className="h-[1.2rem] w-[1.2rem]" />
      </Button>
      <Dialog open={open} onOpenChange={setOpen}>
        <DialogContent>
          <DialogHeader>
            <DialogTitle>An initiative</DialogTitle>
            <DialogDescription>
              <video
                autoPlay={true}
                controls={true}
                style={{ width: "500px", height: "500px" }}
              >
                <source src="https://bucket-qijua0.s3.ap-south-1.amazonaws.com/quran14k.mp4" />
              </video>
            </DialogDescription>
          </DialogHeader>
          <DialogContent></DialogContent>
        </DialogContent>
      </Dialog>
    </>
  );
}
