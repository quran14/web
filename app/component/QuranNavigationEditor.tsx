"use client";
import { Input } from "@/components/ui/input";
import { CheckCircledIcon } from "@radix-ui/react-icons";
import { useRouter } from "next/navigation";
import { useState } from "react";

export default function QuranNavigationEditor({
  latestQuranNo,
}: {
  latestQuranNo: number;
}) {
  const [quranNo, setQuranNo] = useState("");
  const router = useRouter();

  return (
    <div className="flex flex-row space-x-4 items-center">
      <Input
        value={quranNo}
        placeholder={`Go`}
        onChange={(e) => {
          setQuranNo(e.target.value);
        }}
        className={`border border-foreground`}
      />

      {Number(quranNo) <= latestQuranNo && (
        <CheckCircledIcon
          onClick={() => {
            if (Number(quranNo) === latestQuranNo) {
              router.push(`/${quranNo}`);
            } else {
              router.push(`/${quranNo}?latest=${latestQuranNo}`);
            }
          }}
          className={`w-8 h-8  ${
            quranNo.length !== 0
              ? `cursor-pointer hover:text-blue-600 animate-pulse`
              : `text-secondary`
          }`}
        />
      )}
    </div>
  );
}
