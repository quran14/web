import { Button } from "@/components/ui/button";
import { isToday } from "date-fns";
import { format, utcToZonedTime } from "date-fns-tz";
import { ArrowLeftCircle } from "lucide-react";
import Link from "next/link";
import { GetAllJuzzResponse } from "./api/all/route";
import MainTabs from "./component/MainTabs";
import QuranTable from "./component/QuranTable";

export const dynamic = "force-static",
  dynamicParams = true;

async function getData() {
  const url = process.env.NEXT_PUBLIC_URL ?? "http://localhost:3000/";
  const res = await fetch(`${url}/api/all`, {
    cache: "no-cache",
  });
  const data: GetAllJuzzResponse = await res.json();
  console.log("Response: ", data);
  return data;
}

export function formatDate(created: Date) {
  // Convert to IST using the 'Asia/Kolkata' time zone
  const istDate = utcToZonedTime(created, "Asia/Kolkata");

  if (isToday(istDate)) {
    return "Today at " + format(new Date(istDate), "h:mm a");
  } else {
    return "on " + format(new Date(istDate), "d MMMM - h:mm a");
  }
}

export default async function Home() {
  const response = await getData();
  if (!response.success) {
    return (
      <main className="flex min-h-screen flex-col md:items-center p-4">
        <p>Something went wrong, Please try again later!</p>
      </main>
    );
  }

  // Filter out the booked slots
  const filledSlots = response.juzz?.filter((juzz) => juzz.bookedBy !== null);

  // Get the number of filled slots
  const numberOfFilledSlots = filledSlots?.length ?? 0;

  return (
    <main className="flex min-h-screen flex-col md:items-center p-4">
      <div className="justify-center self-center items-center flex-col">
        <MainTabs latestQuranNo={response.quranNo ?? 1} />
      </div>

      <div className="p-2 justify-center items-center flex-col">
        <p className="text-xl text-center font-semibold tracking-wide">
          Quran No.{response.quranNo}
        </p>
        <p className="text-base text-center">
          {numberOfFilledSlots}/30 Juzz confirmed
        </p>

        {response?.createdAt && (
          <time dateTime={new Date(response.createdAt).toDateString()}>
            <p className="text-sm text-center font-mono">
              Started {formatDate(new Date(response.createdAt))}
            </p>
          </time>
        )}
      </div>

      <div className="justify-center self-center items-center flex">
        <Link
          href={
            response.quranNo
              ? `/${(response.quranNo - 1).toString()}?latest=${
                  response.quranNo
                }`
              : "/1"
          }
        >
          <Button className="m-2" variant={"outline"}>
            <div className="flex flex-row space-x-2 items-center">
              <ArrowLeftCircle className="w-4 h-4" />
              <p>Previous</p>
            </div>
          </Button>
        </Link>
      </div>

      <QuranTable juzz={response.juzz} />
    </main>
  );
}
