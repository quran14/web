-- CreateTable
CREATE TABLE "quran" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "singlePerson" BOOLEAN NOT NULL DEFAULT false
);

-- CreateTable
CREATE TABLE "juzz" (
    "id" TEXT NOT NULL,
    "number" INTEGER NOT NULL,
    "name" TEXT NOT NULL,
    "bookedBy" TEXT,
    "quranId" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "juzz_id_key" ON "juzz"("id");

-- CreateIndex
CREATE INDEX "juzz_quranId_idx" ON "juzz"("quranId");
